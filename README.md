# README #

### Twitter API test task implementation ###
* Get all tweets per user
* Find all tweets with images
* Save the fetched tweets in JSON/XML
* Save all associated external assets like image or video

### Tools ###
* [Apache maven](https://github.com/yusuke/twitter4j) - Build system.
* [Git](https://github.com/yusuke/twitter4j) - CVS.

### Libraries ###
* [Twitter4j](https://github.com/yusuke/twitter4j) - Twitter API interaction.
* [Guice](https://github.com/google/guice) - Dependency management.
* [Spark Framework](http://sparkjava.com/) - REST endpoint.
* [Lombok](http://sparkjava.com/) - Java utility library.
* [Gson](https://github.com/google/gson) - Converter to JSON format.
* [JSON-java](https://github.com/stleary/JSON-java) - Converter to XML format (from JSON).
* [Apache commons io](https://commons.apache.org/proper/commons-io/) - Network/filesystem interactions.
* [Apache commons](https://commons.apache.org/proper/commons-lang/) - Common utilities.
* Slf4j, logback - logging utilities.

### How do I get set up? ###
//TODO
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###
* Author: Alexander Davliatov
* email: alexander.davliatov@gmail.com
* skype: alex_davljatov
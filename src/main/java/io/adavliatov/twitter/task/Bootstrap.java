package io.adavliatov.twitter.task;

import com.google.inject.Guice;
import com.google.inject.Injector;
import io.adavliatov.twitter.task.conf.IAppConf;
import io.adavliatov.twitter.task.rest.TwitterAPIResource;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.function.Function;

import static spark.Spark.setIpAddress;
import static spark.Spark.setPort;
import static spark.SparkBase.staticFileLocation;

/**
 * The entry point to the application, launcher class.
 *
 * @author adavliatov
 * @since 08.07.2016
 */
public class Bootstrap {
    public static void main(String[] args) {
        new Bootstrap().start();
    }

    public void start() {
        //inject Guice and get configs.
        Injector injector = Guice.createInjector(new BootstrapModule());
        final IAppConf appConf = injector.getInstance(IAppConf.class);
        String appHost = appConf.getPropertyValue("rest.host", "localhost", Function.identity());
        int appPort = appConf.getPropertyValue("rest.host", 9000, NumberUtils::createInteger);

        //setup Spark
        setIpAddress(appHost);
        setPort(appPort);
        staticFileLocation("/static");

        //configure REST - endpoint
        final TwitterAPIResource twitterAPIResource = injector.getInstance(TwitterAPIResource.class);
        twitterAPIResource.setupEndpoints();
    }
}

package io.adavliatov.twitter.task;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import io.adavliatov.twitter.task.conf.AppConf;
import io.adavliatov.twitter.task.conf.IAppConf;
import io.adavliatov.twitter.task.rest.ITwitterAPIResource;
import io.adavliatov.twitter.task.rest.TwitterAPIResource;
import io.adavliatov.twitter.task.service.handle.ITwitterHandleService;
import io.adavliatov.twitter.task.service.api.ITwitterAPIService;
import io.adavliatov.twitter.task.service.handle.TwitterHandleService;
import io.adavliatov.twitter.task.service.api.TwitterAPIService;
import io.adavliatov.twitter.task.service.storage.FileStorageService;
import io.adavliatov.twitter.task.service.storage.IFileStorageService;
import io.adavliatov.twitter.task.service.storage.IStorageService;
import io.adavliatov.twitter.task.service.storage.StorageService;
import io.adavliatov.twitter.task.service.storage.dao.ITweetDao;
import io.adavliatov.twitter.task.service.storage.dao.TweetDao;
import io.adavliatov.twitter.task.util.FileUtils;
import io.adavliatov.twitter.task.util.IFileUtils;

/**
 * @author adavliatov
 * @since 08.07.2016
 */
class BootstrapModule extends AbstractModule {
    @Override
    protected void configure() {
        //configuration
        bind(IAppConf.class).to(AppConf.class).in(Singleton.class);
        //rest endpoint
        bind(ITwitterAPIResource.class).to(TwitterAPIResource.class);
        //services
        bind(ITwitterAPIService.class).to(TwitterAPIService.class).in(Singleton.class);
        bind(IStorageService.class).to(StorageService.class).in(Singleton.class);
        bind(ITwitterHandleService.class).to(TwitterHandleService.class).in(Singleton.class);
        bind(IFileStorageService.class).to(FileStorageService.class).in(Singleton.class);
        //dao (actually we should consider sharding and multiple instances, but for simplicity)
        bind(ITweetDao.class).to(TweetDao.class).in(Singleton.class);
        //helper
        bind(IFileUtils.class).to(FileUtils.class);
    }
}

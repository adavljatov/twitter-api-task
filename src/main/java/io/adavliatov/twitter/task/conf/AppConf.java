package io.adavliatov.twitter.task.conf;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import io.adavliatov.twitter.task.util.IFileUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.URL;
import java.util.Properties;
import java.util.function.Function;

/**
 * @author adavliatov
 * @since 08.07.2016
 */
@Slf4j
public class AppConf implements IAppConf {
    private final Properties properties;
    private final File storageDir;

    @Inject
    public AppConf(IFileUtils fileUtils) {
        properties = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        final URL resource = classLoader.getResource("app_conf.properties");
        if (resource != null) {
            File file = new File(resource.getFile());
            InputStream input;
            try {
                input = new BufferedInputStream(new FileInputStream(file));
                properties.load(input);
            } catch (IOException e) {
                log.error("Error while reading property file:", e);
            }
        }

        storageDir = fileUtils.createOrCheckDirectory(null, properties.getProperty("storage.location", "twitter-storage")).getDirectory();
    }

    @Override
    public File getStorageDir() {
        return storageDir;
    }

    @Override
    public <V> V getPropertyValue(String key, V defaultVal, Function<String, V> converter) {
        return Optional.fromNullable(converter.apply(properties.getProperty(key))).or(defaultVal);
    }
}

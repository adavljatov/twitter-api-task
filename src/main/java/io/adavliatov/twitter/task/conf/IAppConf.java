package io.adavliatov.twitter.task.conf;

import java.io.File;
import java.util.function.Function;

/**
 * @author adavliatov
 * @since 08.07.2016
 */
public interface IAppConf {
    File getStorageDir();
    <V> V getPropertyValue(String key, V defaultVal, Function<String, V> converter);
}

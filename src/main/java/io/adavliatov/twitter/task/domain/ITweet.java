package io.adavliatov.twitter.task.domain;

import twitter4j.ExtendedMediaEntity;
import twitter4j.Status;

import java.util.Collection;

/**
 * Represents Tweet entity. Wraps twitter4j.Status.
 *
 * @see <a href="http://twitter4j.org/javadoc/twitter4j/Status.html">twitter4j.Status</a>
 * @author adavliatov
 * @since 08.07.2016
 */
public interface ITweet extends Comparable<ITweet> {
    /**
     * NULL-object pattern implementation
     */
    ITweet EMPTY = new ITweet() {
        @Override
        public Long getId() {
            return -1L;
        }

        @Override
        public String getUser() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Status getStatus() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Collection<String> addToMedia(String... mediaPath) {
            throw new UnsupportedOperationException();
        }

        @Override
        public String toJson() {
            throw new UnsupportedOperationException();
        }

        @Override
        public String toXML() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean isEmpty() {
            return true;
        }
    };

    /**
     * Tweet id
     *
     * @return tweet id
     */
    Long getId();

    /**
     * User nick
     * @return tweet user nick
     */
    String getUser();

    /**
     * Corresponding Status entity
     * @return tweet user nick
     */
    Status getStatus();

    /**
     * Convert tweet to JSON
     * @return tweet in JSON format
     */
    String toJson();

    /**
     * Convert tweet to XML
     * @return tweet in XML format
     */
    String toXML();

    /**
     * Add media path(s) to tweet media paths collection
     * @param mediaPath paths to insert
     * @return media path
     */
    Collection<String> addToMedia(String... mediaPath);

    default int compareTo(ITweet that) {
        return this.getId().compareTo(that.getId());
    }

    /**
     * Determines whether we deal with empty Tweet
     * @return true if current tweet is empty
     */
    default boolean isEmpty() {
        return false;
    }
}

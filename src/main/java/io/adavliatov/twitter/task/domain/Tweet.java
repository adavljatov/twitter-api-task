package io.adavliatov.twitter.task.domain;

import com.google.common.base.Optional;
import lombok.Getter;
import lombok.NonNull;
import org.json.XML;
import twitter4j.JSONObject;
import twitter4j.Status;

import java.util.Arrays;
import java.util.Collection;

public final class Tweet implements ITweet {
    @Getter @NonNull private Status tweet;

    @Getter private String nick;

    @Getter
    private Collection<String> mediaPaths;

    private Tweet(Status tweet, String nick, Collection<String> mediaPaths) {
        this.tweet = tweet;
        this.nick = nick;
        this.mediaPaths = mediaPaths;
    }

    @Override
    public Long getId() {
        return tweet != null ? tweet.getId() : -1L;
    }

    @Override
    public String getUser() {
        return Optional.fromNullable(nick).or(tweet.getUser().getName());
    }

    @Override
    public Status getStatus() {
        return tweet;
    }

    public static TweetBuilder builder() {
        return new TweetBuilder();
    }

    @Override
    public String toJson() {
        return new JSONObject(tweet).toString();
    }

    @Override
    public String toXML() {
        final JSONObject tweetAsJson = new JSONObject(tweet);
        org.json.JSONObject tweetJsonToXml = new org.json.JSONObject(tweetAsJson.toString());
        return XML.toString(tweetJsonToXml);
    }

    @Override
    public Collection<String> addToMedia(String... mediaPath) {
        mediaPaths.addAll(Arrays.asList(mediaPath));
        return mediaPaths;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tweet tweet1 = (Tweet) o;

        return tweet.equals(tweet1.tweet);
    }

    @Override
    public int hashCode() {
        return tweet.hashCode();
    }

    /**
     * Tweet builder class
     */
    public static class TweetBuilder {
        private Status tweet;
        private String nick;
        private Collection<String> mediaPaths;

        public TweetBuilder tweet(Status tweet) {
            this.tweet = tweet;
            return this;
        }

        public TweetBuilder nick(String nick) {
            this.nick = nick;
            return this;
        }

        public TweetBuilder mediaPaths(Collection<String> mediaPaths) {
            this.mediaPaths = mediaPaths;
            return this;
        }

        public Tweet build() {
            return new Tweet(tweet, nick, mediaPaths);
        }
    }
}

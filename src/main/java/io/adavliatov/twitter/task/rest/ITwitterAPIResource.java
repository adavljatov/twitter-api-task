package io.adavliatov.twitter.task.rest;

/**
 * Created by Aleksandr_Davliatov on 7/7/2016.
 */
public interface ITwitterAPIResource {
    void setupEndpoints();
}
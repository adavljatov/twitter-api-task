package io.adavliatov.twitter.task.rest;

import com.google.gson.Gson;
import com.google.inject.Inject;
import io.adavliatov.twitter.task.service.storage.IStorageService;
import lombok.extern.slf4j.Slf4j;
import spark.ResponseTransformer;

import static spark.Spark.get;

/**
 * Rest endpoint.
 *
 * @author adavliatov
 * @since 08.07.2016
 */
@Slf4j
public class TwitterAPIResource implements ITwitterAPIResource {
    private static final String API_PREFIX = "/api/";
    private IStorageService storageService;

    @Inject
    public TwitterAPIResource(IStorageService storageService) {
        this.storageService = storageService;
    }

    @Override
    public void setupEndpoints() {
        log.info("List already processed users via `/users` url");
        log.info("List user tweets via `/tweets/:nick` url");
        get(API_PREFIX + "/users", "application/json",
                (request, response) -> {
                    return storageService.listProcessedUsers();
                }, new JsonTransformer());

        get(API_PREFIX + "/tweets/:nick", "application/json",
                (request, response) -> {
                    String nick = request.params(":nick");
                    return storageService.listTweets(nick);
                }, new JsonTransformer());
    }

    /**
     * Get JSON to wrap the response.
     */
    private static class JsonTransformer implements ResponseTransformer {
        private Gson gson = new Gson();

        @Override
        public String render(Object model) {
            return gson.toJson(model);
        }
    }
}
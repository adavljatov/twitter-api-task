package io.adavliatov.twitter.task.service.api;

import io.adavliatov.twitter.task.domain.ITweet;
import twitter4j.Paging;

import java.util.List;

/**
 * Twitter API interaction
 *
 * @author adavliatov
 * @since 08.07.2016
 */
public interface ITwitterAPIService {
    List<ITweet> getTweets(String nick, Paging page);
}
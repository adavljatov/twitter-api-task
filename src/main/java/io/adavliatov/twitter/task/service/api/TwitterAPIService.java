package io.adavliatov.twitter.task.service.api;

import io.adavliatov.twitter.task.domain.ITweet;
import io.adavliatov.twitter.task.domain.Tweet;
import lombok.extern.slf4j.Slf4j;
import twitter4j.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class TwitterAPIService implements ITwitterAPIService {

    private final Twitter twitter = new TwitterFactory().getInstance();

    public List<ITweet> getTweets(String nick, Paging page) {
        ResponseList<Status> userTimeline;
        try {
            userTimeline = twitter.getUserTimeline(nick, page);
        } catch (TwitterException e) {
            log.error("Failed to search tweets: ", e);
            return Collections.emptyList();
        }

        return userTimeline.stream().
                map(status -> Tweet.builder().nick(nick).tweet(status).mediaPaths(new ArrayList<>(status.getExtendedMediaEntities().length)).build()).
                collect(Collectors.toList());
    }

}

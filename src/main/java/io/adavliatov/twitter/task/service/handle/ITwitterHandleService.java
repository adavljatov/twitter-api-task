package io.adavliatov.twitter.task.service.handle;

import io.adavliatov.twitter.task.domain.ITweet;
import io.adavliatov.twitter.task.domain.Tweet;

import java.util.List;
import java.util.Set;

/**
 * The service, responsible for Twitter API interaction results handling (in a multithreading manner)
 *
 * @author adavliatov
 * @since 08.07.2016
 */
public interface ITwitterHandleService {

    /**
     * Loads user tweets via twitter4j and handles if necessary
     *
     * @param nick - the user to process
     * @param last - the last processed tweet
     * @return the list of user tweets
     */
    List<ITweet> getUserTweets(String nick, ITweet last);
}

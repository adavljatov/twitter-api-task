package io.adavliatov.twitter.task.service.handle;

import com.google.inject.Inject;
import io.adavliatov.twitter.task.conf.IAppConf;
import io.adavliatov.twitter.task.domain.ITweet;
import io.adavliatov.twitter.task.service.api.ITwitterAPIService;
import io.adavliatov.twitter.task.service.api.TwitterAPIService;
import io.adavliatov.twitter.task.service.storage.IFileStorageService;
import io.adavliatov.twitter.task.util.DirectoryCreationResult;
import io.adavliatov.twitter.task.util.IFileUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import twitter4j.ExtendedMediaEntity;
import twitter4j.Paging;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Slf4j
public class TwitterHandleService implements ITwitterHandleService {
    private final int taskExecutionTimeout = 1;
    private final ExecutorService dispatcher = Executors.newSingleThreadExecutor();
    private final ExecutorService executor = Executors.newWorkStealingPool();
    private final ConcurrentHashMap<String, Future<List<Future<ITweet>>>> cache = new ConcurrentHashMap<>();
    private final IAppConf appConf;
    private final TwitterAPIService apiService;
    private final IFileStorageService fileStorageService;
    private final IFileUtils fileUtils;

    @Inject
    public TwitterHandleService(IAppConf appConf, TwitterAPIService apiService, IFileStorageService fileStorageService, IFileUtils fileUtils) {
        this.appConf = appConf;
        this.apiService = apiService;
        this.fileStorageService = fileStorageService;
        this.fileUtils = fileUtils;
    }

    @Override
    public List<ITweet> getUserTweets(String nick, ITweet last) {
        final Future<List<Future<ITweet>>> listFuture = cache.compute(nick, (nickName, task) -> {
            if (task == null || task.isDone() || task.isCancelled()) {
                final UserHandleTask userHandleTask = UserHandleTask.of(nick, last, appConf.getStorageDir(), apiService, fileStorageService, fileUtils, executor);
                return dispatcher.submit(userHandleTask);
            }
            return task;
        });
        try {
            //todo: timeout
            return listFuture.get().stream().map(voidFuture -> {
                try {
                    return voidFuture.get(taskExecutionTimeout, TimeUnit.MINUTES);
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    return null;
                }
            }).filter(Objects::nonNull).collect(Collectors.toList());
        } catch (InterruptedException | ExecutionException ignored) {
            return Collections.emptyList();
        }
    }

    @AllArgsConstructor(staticName = "of")
    private static final class UserHandleTask implements Callable<List<Future<ITweet>>> {
        private String nick;
        private ITweet last;
        private File storageDir;
        private ITwitterAPIService apiService;
        private IFileStorageService fileStorageService;
        private IFileUtils fileUtils;
        private ExecutorService executor;

        @Override
        public List<Future<ITweet>> call() {
            final File userDir = fileUtils.createOrCheckDirectory(storageDir, nick).getDirectory();
            List<Future<ITweet>> futures = new ArrayList<>();
            Paging paging = last.isEmpty() ? new Paging(1, 100) : new Paging(1, 100, last.getId());
            List<ITweet> userTimeline;
            do {
                userTimeline = apiService.getTweets(nick, paging);
                final List<Future<ITweet>> chunkTweets = userTimeline.stream().
                        map(tweet -> executor.submit(new TweetHandleTask(tweet, userDir, fileStorageService, fileUtils))).
                        collect(Collectors.toList());
                futures.addAll(chunkTweets);
                paging.setPage(paging.getPage() + 1);
            } while (userTimeline.size() > 0);

            return futures;
        }
    }

    @AllArgsConstructor(staticName = "of")
    private static final class TweetHandleTask implements Callable<ITweet> {
        private final ITweet tweet;
        private final File userDir;
        private final IFileStorageService fileStorageService;
        private final IFileUtils fileUtils;

        @Override
        public ITweet call() {
            final DirectoryCreationResult creationResult = fileUtils.createOrCheckDirectory(userDir, String.valueOf(tweet.getId()));
            if (!creationResult.isNewlyCreated()) {
                return tweet;
            }
            final File tweetDir = creationResult.getDirectory();

            fileUtils.writeToFile(tweetDir, "tweet.json", tweet.toJson());
            fileUtils.writeToFile(tweetDir, "tweet.xml", tweet.toXML());

            final ExtendedMediaEntity[] extendedMediaEntities = tweet.getStatus().getExtendedMediaEntities();
            if (extendedMediaEntities.length == 0) {
                return tweet;
            }

            final File mediaDir = fileUtils.createOrCheckDirectory(tweetDir, "media").getDirectory();
            for (ExtendedMediaEntity extMediaEntity : extendedMediaEntities) {
                final String mediaURL = extMediaEntity.getMediaURL();
                tweet.addToMedia(fileStorageService.saveMedia(mediaURL, mediaDir));
            }
            return tweet;
        }
    }
}

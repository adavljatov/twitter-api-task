package io.adavliatov.twitter.task.service.storage;

import com.google.inject.Inject;
import io.adavliatov.twitter.task.conf.IAppConf;
import io.adavliatov.twitter.task.domain.ITweet;
import io.adavliatov.twitter.task.domain.Tweet;
import io.adavliatov.twitter.task.util.DirectoryCreationResult;
import io.adavliatov.twitter.task.util.IFileUtils;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.math.NumberUtils;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

import java.awt.geom.IllegalPathStateException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Service, responsible for media files storage.
 *
 * @author adavliatov
 * @since 08.07.2016
 */
@Slf4j
public class FileStorageService implements IFileStorageService {
    private final IAppConf appConf;
    private final IFileUtils fileUtils;

    @Inject
    public FileStorageService(IAppConf appConf, IFileUtils fileUtils) {
        this.appConf = appConf;
        this.fileUtils = fileUtils;
    }

    /**
     * Uploads the media file by its URL and save to the provided directory.
     * We 'trust' twitter and assume no *.exe content under ../*.jpg URL. (OR we can make an extra request via HttpURLConnection and check the file Content Type)
     *
     * @param mediaURL media url to download
     * @param mediaDir directory to save media
     * @return mediaPath (mediaId)
     */
    @Override
    public String saveMedia(@NonNull String mediaURL, @NonNull File mediaDir) {
        final String[] split = mediaURL.split("/");
        final String mediaFileName = split[split.length - 1];
        final File mediaFile = new File(mediaDir, mediaFileName);
        int connectionTimeout = appConf.getPropertyValue("connection.timeout", TimeUnit.SECONDS.toMillis(200), NumberUtils::createLong).intValue();
        int readTimeout = appConf.getPropertyValue("read.timeout", TimeUnit.SECONDS.toMillis(200), NumberUtils::createLong).intValue();
        try {
            FileUtils.copyURLToFile(new URL(mediaURL), mediaFile, connectionTimeout, readTimeout);
        } catch (IOException e) {
            log.error("Error during media saving: ", e);
        }
        return mediaFile.getAbsolutePath();
    }

    /**
     * List processed users.
     *
     * @return the list of processed users (nicks)
     */
    @Override
    public List<String> listProcessedUsers() {
        return listProcessedItems(appConf.getStorageDir(), File::getName);
    }


    /**
     * Loads processed tweets from filesystem to cache.
     *
     * @param nick - user nick to process
     * @return the collection of already processed tweet
     */
    @Override
    public List<ITweet> listProcessedTweets(String nick) {
        final DirectoryCreationResult creationResult = fileUtils.createOrCheckDirectory(appConf.getStorageDir(), nick);
        if (creationResult.isNewlyCreated()) {
            return Collections.emptyList();
        }
        File userDir = creationResult.getDirectory();
        Function<File, List<String>> mapToMediaPaths = f -> {
            if (f.listFiles() == null || f.listFiles().length == 0) {
                return Collections.emptyList();
            }
            final DirectoryCreationResult mediaDir = fileUtils.createOrCheckDirectory(f, "media");
            if (mediaDir.isNewlyCreated()) {
                return Collections.emptyList();
            }
            return Arrays.stream(mediaDir.getDirectory().listFiles()).map(File::getAbsolutePath).collect(Collectors.toList());
        };
        Function<File, Status> mapToStatus = f -> {
            try {
                File jsonFile = new File(f, "tweet.json");
                if (!jsonFile.exists()|| jsonFile.isDirectory()) {
                    throw new IllegalPathStateException("Invalid path: " + jsonFile.getAbsolutePath());
                }
                return TwitterObjectFactory.createStatus(FileUtils.readFileToString(jsonFile));
            } catch (IOException | TwitterException e) {
                log.error("Error while reading tweet info from db:", e);
                return null;
            }
        };
        return listProcessedItems(userDir, Function.identity()).
                stream().
                map(f -> Tweet.
                        builder().
                        tweet(mapToStatus.apply(f)).
                        nick(nick).
                        mediaPaths(mapToMediaPaths.apply(f)).build()).
                sorted().
                collect(Collectors.toList());
    }

    /**
     * List tweet's media paths
     *
     * @param userDir - the user directory (contains every loaded tweet)
     * @param tweet - tweet id to process
     * @return the path to the tweet media files
     */
    @Override
    public List<String> listTweetMedia(File userDir, String tweet) {
        return listProcessedItems(fileUtils.createOrCheckDirectory(userDir, tweet).getDirectory(), File::getAbsolutePath);
    }

    private <V> List<V> listProcessedItems(final File dir, Function<File, V> converter) {
        if (dir == null || !dir.exists() || !dir.isDirectory()) {
            throw new IllegalArgumentException("Invalid directory path");
        }
        if (dir.listFiles() == null) {
            return Collections.emptyList();
        }
        return Arrays.stream(dir.listFiles()).map(converter).collect(Collectors.toList());
    }
}
package io.adavliatov.twitter.task.service.storage;

import io.adavliatov.twitter.task.domain.ITweet;
import lombok.NonNull;

import java.io.File;
import java.util.List;

/**
 * Service, responsible for media files storage.
 *
 * @author adavliatov
 * @since 08.07.2016
 */
public interface IFileStorageService {

    /**
     * Uploads the media file by its URL and save to the provided directory.
     * We 'trust' twitter and assume no *.exe content under ../*.jpg URL. (OR we can make an extra request via HttpURLConnection and check the file Content Type)
     *
     * @param mediaURL media url to download
     * @param mediaDir directory to save media
     * @return mediaName (mediaId)
     */
    String saveMedia(@NonNull String mediaURL, @NonNull File mediaDir);

    /**
     * List already processed users.
     *
     * @return the list of processed users (nicks)
     */
    List<String> listProcessedUsers();

    /**
     * Loads already processed tweets from filesystem to the cache.
     *
     * @param nick - user nick to process
     * @return the collection of already processed tweet
     */
    List<ITweet> listProcessedTweets(String nick);

    /**
     * List tweet's media paths
     *
     * @param userDir - the user directory (contains every loaded tweet)
     * @param tweet - tweet id to process
     * @return the path to the tweet media files
     */
    List<String> listTweetMedia(File userDir, String tweet);
}
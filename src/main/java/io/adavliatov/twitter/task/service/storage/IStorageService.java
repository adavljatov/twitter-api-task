package io.adavliatov.twitter.task.service.storage;

import io.adavliatov.twitter.task.domain.ITweet;
import io.adavliatov.twitter.task.domain.Tweet;

import java.util.List;
import java.util.Set;

/**
 * Service, responsible for the request execution handling.
 *
 * @author adavliatov
 * @since 10.07.2016
 */
public interface IStorageService {

    /**
     * List of already processed users
     *
     * @return the set of processed users (nicks).
     */
    List<String> listProcessedUsers();

    /**
     * Get user tweets and their associated media files.
     *
     * @param nick - the user nick to process
     * @return the list of user tweets
     */
    List<ITweet> listTweets(String nick);
}

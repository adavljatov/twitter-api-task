package io.adavliatov.twitter.task.service.storage;

import com.google.inject.Inject;
import io.adavliatov.twitter.task.domain.ITweet;
import io.adavliatov.twitter.task.domain.Tweet;
import io.adavliatov.twitter.task.service.storage.dao.ITweetDao;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service, responsible for the request execution handling.
 *
 * @author adavliatov
 * @since 10.07.2016
 */
public class StorageService implements IStorageService {
    private final ITweetDao tweetDao;

    @Inject
    public StorageService(ITweetDao tweetDao) {
        this.tweetDao = tweetDao;
    }

    /**
     * List of already processed users
     *
     * @return the set of processed users (nicks).
     */
    @Override
    public List<String> listProcessedUsers() {
        return tweetDao.listUser();
    }

    /**
     * Get user tweets and their associated media files.
     *
     * @param nick - the user nick to process
     * @return the list of user tweets
     */
    @Override
    public List<ITweet> listTweets(String nick) {
        return tweetDao.listUserTweets(nick).stream().sorted().collect(Collectors.toList());
    }
}

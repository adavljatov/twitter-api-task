package io.adavliatov.twitter.task.service.storage.dao;

import io.adavliatov.twitter.task.domain.ITweet;
import io.adavliatov.twitter.task.domain.Tweet;

import java.util.List;
import java.util.Set;

/**
 * Keeps the of processed users and their tweets
 *
 * @author adavliatov
 * @since 10.07.2016
 */
public interface ITweetDao {

    /**
     * Loads the tweet to the cache
     *
     * @param tweet the tweet to save
     * @return the collection of user processed tweets
     */
    List<ITweet> saveTweet(ITweet tweet);

    /**
     * List processed users (nicks)
     *
     * @return the collection of processed users
     */
    List<String> listUser();

    /**
     * List user tweets and updates if necessary.
     *
     * @param nick user to process
     * @return the collection of processed tweets
     */
    List<ITweet> listUserTweets(String nick);
}

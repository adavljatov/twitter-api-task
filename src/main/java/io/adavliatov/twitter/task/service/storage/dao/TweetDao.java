package io.adavliatov.twitter.task.service.storage.dao;

import com.google.inject.Inject;
import io.adavliatov.twitter.task.domain.ITweet;
import io.adavliatov.twitter.task.service.handle.TwitterHandleService;
import io.adavliatov.twitter.task.service.storage.IFileStorageService;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class TweetDao implements ITweetDao {
    private ConcurrentHashMap<String, List<ITweet>> cache = new ConcurrentHashMap<>();
    private final TwitterHandleService twitterHandleService;

    @Inject
    public TweetDao(TwitterHandleService twitterHandleService, IFileStorageService fileStorageService) {
        final List<String> nicks = fileStorageService.listProcessedUsers();
        nicks.stream().forEach(nick -> cache.put(nick, fileStorageService.listProcessedTweets(nick)));
        this.twitterHandleService = twitterHandleService;
    }

    @Override
    public List<ITweet> saveTweet(ITweet tweet) {
        return cache.compute(tweet.getUser(), (nick, tweets) -> {
            tweets.add(tweet);
            return tweets;
        });
    }

    @Override
    public List<String> listUser() {
        return cache.keySet().stream().collect(Collectors.toList());
    }


    @Override
    public List<ITweet> listUserTweets(String nick) {
        return cache.compute(nick, (nickName, tweets) -> {
            tweets = Optional.ofNullable(tweets).orElse(new ArrayList<>());
            final ITweet last = tweets.isEmpty() ? ITweet.EMPTY : tweets.get(tweets.size() - 1);
            tweets.addAll(twitterHandleService.getUserTweets(nick, last));
            return tweets;
        });
    }
}

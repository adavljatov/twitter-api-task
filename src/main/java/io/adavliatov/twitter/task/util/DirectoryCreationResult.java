package io.adavliatov.twitter.task.util;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.File;

/**
 * Represent directory creation result.
 *
 * @author adavliatov
 * @since 08.07.2016
 */
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public final class DirectoryCreationResult {
    @Getter
    private File directory;
    @Getter
    private boolean newlyCreated;
}
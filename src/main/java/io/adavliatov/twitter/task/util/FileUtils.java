package io.adavliatov.twitter.task.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

/**
 * @author adavliatov
 * @since 10.07.2016
 */
@Slf4j
public final class FileUtils implements IFileUtils {
    @Override
    public DirectoryCreationResult createOrCheckDirectory(final File parent, String path) {
        final File dir = parent == null ? new File(path) : new File(parent, path);
        boolean newlyCreated = false;
        if (!dir.exists()) {
            if (dir.mkdir()) {
                newlyCreated = true;
            }
        } else if (!dir.isDirectory()) {
            throw new IllegalStateException("Invalid path: " + dir.getAbsolutePath());
        }
        return new DirectoryCreationResult(dir, newlyCreated);
    }

    @Override
    public void writeToFile(final File tweetDir, final String fileName, final String content) {
        final File tweet = new File(tweetDir, fileName);
        try {
            if (!tweet.exists()) {
                tweet.createNewFile();
            } else if (tweet.isDirectory()) {
                throw new IllegalStateException("Invalid path: " + tweet.getAbsolutePath());
            }
            org.apache.commons.io.FileUtils.writeStringToFile(tweet, content);
        } catch (IOException e) {
            log.error("Error during tweet saving: ", e);
        }
    }
}

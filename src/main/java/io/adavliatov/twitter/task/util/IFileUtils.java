package io.adavliatov.twitter.task.util;

import java.io.File;

/**
 * @author adavliatov
 * @since 10.07.2016
 */
public interface IFileUtils {
    DirectoryCreationResult createOrCheckDirectory(File parent, String path);
    void writeToFile(File tweetDir, String fileName, String content);
}

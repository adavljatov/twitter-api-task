var app = angular.module('twitter-app', [
    'ngResource',
    'ngRoute'
]);

app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'view/tweets.html',
        controller: 'ListTweets'
    }).when('/users', {
        templateUrl: 'view/users.html',
        controller: 'ListUsers'
    }).otherwise({
        redirectTo: '/'
    })
});

app.controller('ListUsers', function ($scope, $http) {
    $http.get('/api/users').success(function (data) {
        $scope.users = data;
    }).error(function (data, status) {
        console.log('Error ' + data)
    });
});

app.controller('ListTweets', function ($scope, $http) {
    $scope.user = {
        nick: ''
    };

    $scope.getTweets = function (user) {
        console.log(user);
        $http.get('/api/tweets/' + user.nick).success(function (data) {
            $scope.tweets = data;
        }).error(function (data, status) {
            console.log('Error ' + data)
        })
    }
});